import { Component , OnInit } from '@angular/core'
import { ProductlistService } from './productList.service'
import { Product} from './product'
import {Observable} from 'rxjs/Observable';
import {Store} from "@ngrx/store";
import { SELECT_PRODUCT , SHOW_PRODUCT_DETAILS } from './product-action';

@Component({
    selector : 'product-list',
    templateUrl : './productlist.component.html'
})

export class ProductListComponent implements OnInit{

    products : Product;
    errorMessage: string;
    productlist:Product[];
    tagState$:Observable<Product[]>;

      constructor(private productListService : ProductlistService,private store:Store<Product>) {
        this.tagState$=store.select('store');
        this.getProduct();
   }

   getProduct(){
        this.productListService.getProducts().subscribe(details=>
        {
            this.store.dispatch({type:SELECT_PRODUCT,payload:details
        })
        console.log(details)
    });
}
 

    
    ngOnInit(){
        this.tagState$.subscribe(p=>
            {console.log(p);this.productlist=p});
    }
    
}