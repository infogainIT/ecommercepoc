import { Injectable } from '@angular/core'
import { Http , Response } from '@angular/http'
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import { Product} from './product'

@Injectable()

export class ProductlistService{

    constructor(private _http : Http){
    }

    getProducts():Observable<Product[]>{
        return this._http.get('http://localhost:4200/assets/productlist.json').map(res => res.json())
        .do(data => console.log(JSON.stringify(data))).catch(this.handleError);
    }

    getProduct(id: number): Observable<Product> {
        return this.getProducts()
            .map((products:Product[]) => products.find(p => p.id === id));
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}