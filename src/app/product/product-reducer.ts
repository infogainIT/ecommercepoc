import { Action } from '@ngrx/store';
import { Product } from './product';
import { SELECT_PRODUCT ,SHOW_PRODUCT_DETAILS } from './product-action';

export function productReducer(state:Product[],action):any {
  switch (action.type) {
    case SELECT_PRODUCT:
      return Object.assign([], state,action.payload);
    case SHOW_PRODUCT_DETAILS:
     let dataOfP;
      state.filter((p)=> {
        if(p.id==action.payload){
          dataOfP =  p;
        }
      }
      );
     return  dataOfP;
    default:
      return state;
  }
}