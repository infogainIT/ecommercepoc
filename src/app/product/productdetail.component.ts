import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ProductlistService } from './productList.service'
import { Product } from './product'
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import {Store} from "@ngrx/store";
import { SHOW_PRODUCT_DETAILS } from './product-action';

@Component({
    selector: 'product-detail',
    templateUrl: './productdetail.component.html'
})

export class ProductDetailComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Product Detail';
    product: Product;
    errorMessage: string;
    private sub: Subscription;
    tagState$:Observable<Product>;

    // constructor(,
     constructor(private _route: ActivatedRoute, private _router: Router,private _productService: ProductlistService ,private store:Store<any>) {
     }


    ngOnInit() {
        this.tagState$ = this.store.select('store');
        this.sub = this._route.params.subscribe(
            params => {
                let id = +params['id'];
                console.log(id);
                 this.getProductDetails(id);
            })
        this.tagState$.subscribe(p =>
        {   console.log('p value from store'); 
            console.log(p); 
            this.product = p }
        );
    };


getProductDetails(id : number){
    this._productService.getProduct(id).subscribe(details => {
        this.store.dispatch({
            type: SHOW_PRODUCT_DETAILS, payload: details.id
        });
        console.log(details);
    });

}
// getProduct(id: number) {
//     this._productService.getProduct(id).subscribe(
//         product => this.product = product);
//         console.log("product details");
//         console.log( this.product);
// }

ngOnDestroy() {
    this.sub.unsubscribe();
}

}
