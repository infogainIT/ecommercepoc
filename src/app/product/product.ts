export class Product{

         imageUrl : string;
         id : number;
         title : string;
         body :string;
         price : number;
         starRating : number;
    constructor(imageUrl="",id=0 , title ="" , body ="",price =0 , starRating = 0  )
    {
            this.imageUrl = imageUrl;
            this.id = id;
            this.title = title;
            this.body = body;
            this.price = price;
            this.starRating = starRating;
    }

}