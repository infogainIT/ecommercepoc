import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

const heroesRoutes: Routes = [
    {
        path: '',
        loadChildren: 'app/admin/admin.module#SidenavModule'
    },
    { path: '', component: DashboardComponent },
    { path: 'dashboard', component: DashboardComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(heroesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class DashboardRoutingModule { }