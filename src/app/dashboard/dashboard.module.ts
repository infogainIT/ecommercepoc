import {NgModule} from '@angular/core';
import { CommonModule }   from '@angular/common';
import {RouterLink} from '@angular/router';

import {DashboardComponent} from './dashboard.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {SidenavComponent} from 'app/sidenav/sidenav.component';
import {SidenavModule} from 'app/sidenav/sidenav.module';
import {SideNavRoutingModule} from 'app/sidenav/sidenav-routing.module'

@NgModule({
   imports: [
    CommonModule,
    SidenavModule
  ],
  declarations: [
    DashboardComponent,
    HeaderComponent,
    FooterComponent],
  exports: [DashboardComponent]
})
export class DashboardModule { }