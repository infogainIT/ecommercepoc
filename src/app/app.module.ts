import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PageNotFoundComponent} from './not-found.component';
import { ProductListComponent } from './product/productlist.component';
import { ProductlistService } from './product/productList.service';
import { ProductDetailComponent } from './product/productdetail.component';
import { HttpModule } from '@angular/http';
import { Store ,StoreModule } from '@ngrx/store';
import { productReducer } from './product/product-reducer';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    DashboardModule,
    AppRoutingModule,
    HttpModule,
    StoreModule.provideStore({ store: productReducer })
  ],
  providers: [ProductlistService],
  bootstrap: [AppComponent]
})
export class AppModule { }
