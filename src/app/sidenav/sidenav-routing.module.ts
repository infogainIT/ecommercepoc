import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FashionComponent } from './fashion.component';
import { WatchesComponent } from './watches.component';
import { ProductListComponent } from 'app/product/productlist.component';
import { ProductDetailComponent } from 'app/product/productdetail.component';
import { GalleryComponent } from 'app/product/gallery.component'
const navRoutes: Routes = [
  {
    path: '',
    component: ProductListComponent,
  },
  {
    path: 'productlist',
    component: ProductListComponent,
  },
  {
    path: 'productdetail/:id',
    component: ProductDetailComponent,
  },
  { path: 'fashion', component: FashionComponent },
  { path: 'watches', component: WatchesComponent },
  { path: 'gallery', component: GalleryComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(navRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class SideNavRoutingModule { }