import { Injectable } from '@angular/core';

import { NavMenu } from './navmenu';



@Injectable()
export class SidenavService {

mockMENU : NavMenu [] = [
                        { url: "/fashion", name: "Fashion" },
                        {url: "/gallery", name:"Gallery"},
                        {url: "/productlist", name:"Products"},
                    ];


    getMenu(): Promise<NavMenu[]> {
        return Promise.resolve(this.mockMENU);
    }
}