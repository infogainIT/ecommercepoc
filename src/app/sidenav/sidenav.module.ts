import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import {SidenavComponent} from 'app/sidenav/sidenav.component';
import {SideNavRoutingModule} from 'app/sidenav/sidenav-routing.module';
import {FashionComponent} from './fashion.component';
import {WatchesComponent} from './watches.component';
import {ProductListComponent} from 'app/product/productlist.component';
import {GalleryComponent} from 'app/product/gallery.component'

@NgModule({
  imports: [
    CommonModule,
    SideNavRoutingModule
  ],
  declarations: [
    SidenavComponent,
    FashionComponent,
    WatchesComponent,
    ProductListComponent,
    GalleryComponent
  ],
  exports : [SidenavComponent, ProductListComponent]

})
export class SidenavModule {}
