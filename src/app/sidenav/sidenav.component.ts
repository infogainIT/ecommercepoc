import { Component, OnInit} from '@angular/core';
//import {ROUTER_DIRECTIVES} from '@angular/common';

import { NavMenu } from './navmenu';
import {SidenavService} from './sidenav.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  providers : [SidenavService]
})
export class SidenavComponent implements OnInit {
  title = 'sidenav';
  myMenu : Promise<NavMenu[]>;

  constructor(
    private service: SidenavService
  ) {}

ngOnInit() {
  this.myMenu = this.service.getMenu();
}
}
