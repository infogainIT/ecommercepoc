import { EcommerceProjectPage } from './app.po';

describe('ecommerce-project App', () => {
  let page: EcommerceProjectPage;

  beforeEach(() => {
    page = new EcommerceProjectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
